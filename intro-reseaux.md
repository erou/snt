## Activité d'introduction

*Cette activité est à réaliser en groupes de 4 ou 5 personnes.*

1. Dresser la *carte d'identité* d'un réseau social sous la forme d'un poster,
   il faudra au minimum faire apparaître les informations suivantes
    + date de création ;
    + pays d'origine ;
    + nom du ou des fondateur(s) ;
    + nombre d'utilisateurs et utilisatrices ;
    + le principe sur lequel il se fonde ;
    + l'entreprise qui le possède ;
    + sa ou ses principales sources de revenus.
2. Mettre en commun les différentes cartes d'identité en les présentant à la
   classe.
3. Commenter le slogan « Si c'est gratuit, c'est vous le produit ! ».

Voici une liste (non exhaustive) de réseaux sur lesquels vous pouvez travailler
: Facebook, X (Twitter), TikTok, LinkedIn, Instagram, WhatsApp, Twitter,
YouTube, Snapchat... Vous pouvez aussi en prendre un que vous ne connaissez pas
sur [cette
liste](https://www.leptidigital.fr/reseaux-sociaux/liste-reseaux-sociaux-14846/)
des 30 réseaux les plus utilisés.

*Activité empruntée au manuel Le livre scolaire.*
