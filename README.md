<div style="text-align:center">
<h2> Science Numérique et Technologie </h2>
<p> Le site de votre cours de SNT. </p>
</div>

---

## <i class="fa fa-cogs" aria-hidden="true"></i> Mode d'emploi

<p>
L'icone
<i class="fa fa-align-justify"></i>
vous permet d'afficher ou de masquer le menu de navigation. Dans ce menu, vous
trouverez des ressources liées aux différents thèmes explorés en SNT.
</p>
<p>
Les flèches directionnelles
<i class="fa fa-angle-left"></i>
et
<i class="fa fa-angle-right"></i>
permettent de naviguer sur le site. Sur ordinateur, vous pouvez également
utiliser votre clavier.
</p>
<p>
L'icone
<i class="fa fa-font"></i>
vous permet de changer la police d'écriture, sa taille, ou le thème du site.
</p>
<p>
Enfin, la barre de recherche vous permet de taper un mot-clé qui sera cherché dans
l'ensemble du site.
</p>

## <i class="fa fa-envelope" aria-hidden="true"></i> Me contacter

Pour me contacter, le plus simple est de passer par la messagerie interne
de l'ENT. Vous pouvez également m'envoyer un email directement sur mon adresse
académique : edouard.rousseau@ac-versailles.fr.

## <i class="fa fa-compass" aria-hidden="true"></i> Ressources

Les ressources disponibles sur ce site sont largement inspirées de travaux
disponibles sur Internet, je remercie vivement les différentes personnes
impliquées. Voici une liste non exhaustive de sites utiles :

- [Ressources SNT de l'AEIF](https://ressources.forge.apps.education.fr/snt/)
- [Pixees](https://pixees.fr/informatiquelycee/n_site/snt_web_cookies.html)
- [SNT à La Martinière Diderot](https://portail.lyc-la-martiniere-diderot.ac-lyon.fr/srv20/co/_Module_SNT.html)
- [Site personnel de Frederic Junier](https://frederic-junier.org/)
- [Site personnel de dav74](https://dav74.github.io/site_snt)
