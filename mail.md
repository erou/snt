## L'importance de l'email

+ Dans votre vie de lycéen⋅e puis d’étudiant⋅e puis votre vie professionnelle,
  vous allez devoir écrire **des centaines** d’emails.
+ Il y a des règles d’écriture pour un email, qu’il faut connaître,
  sous peine de ne pas être crédible aux yeux de votre destinataire

---

## Règles d'écriture

Certaines parties (salutations en début et fin de mail) dépendent du registre
dans lequel vous écrivez votre email. Ce registre dépend lui-même de votre
relation avec le ou la destinataire. La structure globale peut être la suivante.

1. Votre email doit contenir un objet. **Attention : l’objet d’un email
   est différent du corps du mail.**
2. Corps du mail : il doit commencer par une phrase de salutation.
    + Bonjour Monsieur X,
    + Chère Madame Y,
    + Bonjour à toutes et à tous,
    + ...
3. Vous devez ensuite rédiger **le plus clairement possible** votre message.
    + Indiquer (si besoin) qui vous êtes.
    + Indiquer la raison de votre message
        - En précisant éventuellement le contexte.
        - En étant clair sur ce que vous attendez du ou de la destinataire.
        - En étant poli.
4. Mettre une formule de fin de mail.
    + Cordialement,
    + Bonne fin de journée,
    + Merci pour votre aide,
    + Amicalement,
    + ...
5. Signer (prénom et nom).
