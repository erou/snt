## Mémo Python

Voici un petit récapitulatif des structures vues en classe cette année, et qu'il
est important de connaître pour la suite.

### Calculs et Variables

En python, les calculs "basiques" (addition, soustraction, multiplication,
division) se font avec les opérations `+`, `-`, `*` et `/`. L'exponentiation se
fait avec une double astérisque : `**`.

On peut attribuer une valeur à une variable avec le symbole `=` :

```python
a = 10
```

### Boucles for

Les *boucles for* servent à répéter une tâche un nombre donné de fois.

```python
for i in range(5):
    print(i)
```

affichera

```python
0
1
2
3
4
```

Faites le test !

<iframe src="https://trinket.io/embed/python/06841c2aec" width="100%" height="300" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe>

### Structures conditionnelles

Les structures de type "SI ... ALORS ... SINON ... ALORS ..." utilisent les
mots clés `if` et `else`.

```python
if a >= b:
    print("Le nombre a est supérieur ou égal à b.")
else:
    print("Le nombre a est strictement inférieur à b.")
```

### Boucles while

Les *boucles while* servent à répéter une tâche *tant qu'*une certaine condition
est vérifiée. On ne maîtrise pas forcément à l'avance combien de fois la boucle
va être répétée.

```python
a = 1
while a < 11:
    a = a + 4
    print(a)
```

affichera

```python
5
9
13
```

### Fonctions

Une *fonction* permet de donner un nom à une série d'instructions, qui peuvent
dépendre de paramètres d'entrées (on parle d'*arguments*). Le mot clé `def`
permet de définir une fonction et le mot clé `return` permet de renvoyer un
résultat.

La fonction suivante permet de calculer le prix d'un article soldé en fonction
de son prix de départ et du pourcentage de solde.

```python
def solde(prix, pourcentage):
    remise = pourcentage/100 * prix
    nouveau_prix = prix - remise
    return nouveau_prix
```
