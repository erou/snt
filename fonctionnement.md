## Le fonctionnement des réseaux sociaux

**Pour cette activité, on formera des groupes de cinq personnes.**

1. Répartir les réseaux sociaux de la liste suivante parmi les membres du
   groupe.
    + TikTok
    + Instagram
    + Snapchat
    + X (Twitter)
    + YouTube
2. Regarder la vidéo associée à votre réseau social.
3. Mettre en commun les informations apprises dans la vidéo avec l'ensemble des
   membres du groupe afin de remplir le tableau suivant (*reproduire le tableau
   sur une feuille*).

Réseau|Principes|Fonctionnalités utilisateurs|Données collectées|Mécanismes d'addiction
------|---------|----------------------------|------------------|----------------------
TikTok|         |                            |                  |                       
Instagram|      |                            |                  |                       
Snapchat|       |                            |                  |                       
X (Twitter)|    |                            |                  |                       
YouTube|        |                            |                  |                      

---

<div style="text-align:center">
<h3>TikTok</h3>
<iframe width="560" height="315" src="https://www.youtube.com/embed/-pOWdBpVR3s?si=Jn6cT5GqwGK1_GN1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
<h3>Instagram</h3>
<iframe width="560" height="315" src="https://www.youtube.com/embed/pHl_xZqlI10?si=Xwk06c369jsZVAMv" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
<h3>Snapchat</h3>
<iframe width="560" height="315" src="https://www.youtube.com/embed/fNwLYO2dpW0?si=rxiPTJmKHL4Jm9rx" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
<h3>X (Twitter)</h3>
<iframe width="560" height="315" src="https://www.youtube.com/embed/tPAKmcb1sWU?si=Qhy2K_FXzx4zjv-p" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
<h3>YouTube</h3>
<iframe width="560" height="315" src="https://www.youtube.com/embed/v6ucHJb948s?si=pqXJURln83kGYWFO" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

---

### Synthèse

**Utilisation des données personnelles :**
- les réseaux sociaux proposent de
  nouvelles vidéos (TikTok, YouTube) et analysent les résultats (*likes*, *swipe*,
  durée de visionnage) afin de comprendre quelles vidéos sont susceptible de faire
  le *buzz* et celles qui nous feront passer le plus de temps sur le réseau, le
  but est de maximiser notre *engagement* sur le réseau ;
- les réseaux sociaux (Instagram, X, YouTube) analysent nos abonnements et intéractions
  (messages, *likes*, *retweets*) afin de
  comprendre nos centres d'intérêts, construire un profil d'utilisateur, et
  proposer des publicités ciblées.

**Mécanismes d'addiction :**
- notre cerveau est attiré naturellement par le mouvement et les vidéos ont
  tendance à nous intéresser (Instagram, TikTok, YouTube) ;
- envie de faire le *buzz* (TikTok) ;
- les vidéos de danse et boucles musicales nous donnent envie de participer à un
  phénomène que l'on perçoit comme populaire et que l'on finit par apprécier (TikTok) ;
- les challenges (TikTok) nous permettent de faire partie d'une communauté ;
- les réseaux sociaux (TikTok, Instagram, X) sont également le lieu de la
  *validation sociale* et de l'auto-propagande : on cherche les *likes*,
  *retweets*, *etc.* pour se sentir aimé ;
- on a besoin d'aller sur les réseaux (X) par peur de rater quelque chose (*Fear
  Of Missing Out: FOMO*) ;
- un algorithme (X, YouTube) nous propose que des contenus susceptibles de
  nous intéresser ;
- les vidéos se lancent automatiquement et on a envie d'achever une vidéo
  commencée (YouTube), c'est l'effet
  [Zeigarnik](https://fr.wikipedia.org/wiki/Effet_Zeigarnik) ;
