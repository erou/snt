## Qu'est-ce que le web ?

Et quelle est la différence avec *Internet* ?

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/GqD6AiaRo3U?si=QCn77uvMnaMEz0xE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

### En résumé : le *site* web

Un site web, c'est un **ensemble de fichiers** organisés en dossiers et qui
peuvent être de différents types :

+ texte ;
+ son ;
+ photo ;
+ vidéo ;
+ programme exécutable ;
+ ...

### En résumé : la *page* web

Une page web est un fichier qui permet de publier :

+ du contenu (texte, images, vidéos, ...) : le fond ;
+ un style graphique : la forme.

Le contenu est écrit en **langage HTML** (extension .html) tandis que le
style graphique est écrit en **langage CSS** (extension .css). 

Voici un [premier exemple](https://capytale2.ac-paris.fr/web/c/463a-2880312) de
page web, avec vos premières balises HTML.

### Votre première page

Le but est de créer une [première
page](https://capytale2.ac-paris.fr/web/c/4486-2880310) qui va présenter votre
film préféré.

- Commencez par analyser la structure du fichier (balises `<h1>` et `<br />`)
- Modifiez le fichier .html pour mettre votre nom et le titre de votre film
  préféré
- Ajoutez des balises `<h2>` (titre de rang 2) autour de *Information générale*,
  *Résumé*, *Bande-Annonce du film* et *Sources*.
- Ajoutez les balises `<strong>` autour des 4 items dans Informations générales.
- Complétez les informations manquantes.
- Ajoutez une image : pour ça il faudra télécharger une image dans les fichiers
  puis utiliser la balise `<img src = "nom de l'image.jpg" />`
- Intégrez la bande-annonce : pour cela il suffit de vous rendre par exemple sur
  la bande d'annonce dans YouTube, faire "Partager" puis "Intégrer". Vous aurez
  alors simplement un code à recopier dans votre fichier .html.
- Créez des liens hypertextes, cela se fait avec la balise `<a>` dont la syntaxe
  est `<a href="adresse_du_lien_hypertexte">support_du_lien</a>`. Le support du
  lien peut être constitué de texte ou d'une image par exemple.
- Vous pouvez enfin modifier le fichier .css pour avoir de la couleur par
  exemple. Voici un exemple :

```css
body {
	background-color: pink;
	}

h1 {
	color: blue;
	}

h2 {
	font-variant: small-caps;
	color: green;
	}

strong {
	background-color: gold;
	}
```

### Barême

- Nom, prénom, titre du film : 1 pt
- Balises `<h2>` : 1 pt
- Balises `strong` : 1 pt
- Informations manquantes : 1 pt
- Affiche du film : 1 pt
- Bande-annonce : 1 pt
- Liens hypertexte : 1 pt
- Modification du fichier CSS : 1 pt
- Visibilité globale du site : 1 pt
- Syntaxe HTML respectée : 1 pt
