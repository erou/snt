## Caractéristiques principales

### La définition d'un réseau social

{% hint style='tip' %}
Un **réseau social** est une application mettant en relation des
individus, grâce à Internet.
{% endhint %}

Quelques exemples :
+ [LinkedIn](https://fr.wikipedia.org/wiki/LinkedIn) est un réseau
  social basé sur les relations dans le monde professionnel ;
+ X (anciennement [Twitter](https://fr.wikipedia.org/wiki/Twitter))
  est un réseau social généraliste, mais orienté vers l'actualité ;
+ [Snapchat](https://fr.wikipedia.org/wiki/Snapchat) est basé sur
  l'envoi de photos ou vidéos (appelés *snaps*) ;
+ [TikTok](https://fr.wikipedia.org/wiki/TikTok) est basé sur le partage
  de courtes vidéos ;
+ [Facebook](https://fr.wikipedia.org/wiki/Facebook) est un réseau
  social généraliste.

### Domaines d'applications

Les réseaux sociaux sont utilisés pour des raisons très diverses.
Certains réseaux se concentrent sur certaines fonctionnalités très spécifiques :
+ partage de contenu (vidéo, photo) ;
+ monde professionnel ;
+ microblogging ;
+ messagerie instantannée ;
+ *streaming* vidéo ;
+ *etc.*

D'autres réseaux, comme Facebook et X (Twitter), font le choix d'être
**généralistes**, c'est-à-dire de fournir le plus de fonctionnalités
possibles à leurs utilisateurs et utilisatrices.

### Données personnelles et identité numérique

Pour utiliser un réseau social, il faut s'y inscrire. Cette
inscription requiert le choix d'un identifiant et d'un mot de passe. Parfois,
d'autres informations sont demandées (date de naissance, email personnel, nom,
*etc.*) : toutes ses informations constituent des **données personnelles**.

{% hint style='tip' %}
Le paramétrage des abonnements permet de contrôler la confidentialité de ses
données personnelles et les traces laissées sur les réseaux sociaux.
L'ensemble de ses traces est appelé **identité numérique**. Elle influence l'image
de l'utilisateur sur Internet, appelée **e-réputation**.
{% endhint %}
