## Les cookies

### Activité 1

**À faire en autonomie :**

- regarder les deux vidéos ci-dessous (n'hésitez pas à prendre des notes pendant
  le visionnage) ;
- rédiger un texte de quelques lignes qui résumera les thèmes abordés dans ces
  vidéos ;
- échanger en petits groupes pour résumer les vidéos et vérifier que vous avez
  bien compris les mêmes choses.

<div style="text-align:center">
<iframe frameborder="0" width="560" height="315" src="https://www.dailymotion.com/embed/video/x16lt53" allowfullscreen allow="autoplay"></iframe>
</div>

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/fm5MSdPU8tY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

### Activité 2

**À faire en autonomie :**

À l'aide des informations présentes sur le site de la
[CNIL](https://www.cnil.fr/fr/cookies-les-outils-pour-les-maitriser) et dans la
vidéo ci-dessous, paramétrez votre navigateur web afin de limiter vos traces
lors de vos navigations sur le web.

<div style="text-align:center">
<iframe frameborder="0" width="560" height="315" src="https://www.dailymotion.com/embed/video/xw48jr" allowfullscreen allow="autoplay"></iframe>
</div>
