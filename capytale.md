## La plateforme Capytale

Pour apprendre la programmation, on utilise la plateforme
[Capytale](https://capytale2.ac-paris.fr/web/accueil), qui est accessible
grâce à vos identifiants ENT.

---

## Fonctionnement de Capytale

Dans Capytale, on peut taper des instructions en Python que l'on exécute en
tapant sur \[Ctrl\]+\[Entrée\], ou en cliquant sur le bouton "Exécuter". Il y a
également des cellules de texte **à lire** pour bien comprendre l'activité.

{% hint style='danger' %}
Il est important de **lire tout le texte** lorsque vous faites une activité. Si
vous ne prenez pas cette habitude, vous risquez de ne pas comprendre.
{% endhint %}

---

## Liste des activités Capytale

Nom de l'activité | Code d'accès
------------------|:-----------:
[De Scratch à Python : le chat et la tortue](https://capytale2.ac-paris.fr/web/c/9e7b-1866846)|9e7b-1866846
[Tortue : fonctions, frises, feu d'artifice](https://capytale2.ac-paris.fr/web/c/2f08-1911054)|2f08-1911054
[Bases de Python : premiers calculs et variables](https://capytale2.ac-paris.fr/web/c/08bc-1426851)|08bc-1426851
[Activité Python 2 : boucles for](https://capytale2.ac-paris.fr/web/c/1266-1478512)|1266-1478512
[Boucle while et structure conditionnelle](https://capytale2.ac-paris.fr/web/c/b57e-1533700)|b57e-1533700
[Fonctions](https://capytale2.ac-paris.fr/web/c/def9-1598766)|def9-1598766
[TP sur le binaire](https://capytale2.ac-paris.fr/web/c/ccac-2818814)|ccac-2818814
