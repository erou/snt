## Le cyberharcèlement

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/LSCYn4ZCC-w?si=SuLqAFOcIioy4vYJ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

### Quelques questions pour bien comprendre

+ Le harcèlement en ligne, ou cyberharcèlement, est-il puni par la loi ?
+ Le cyberharcèlement est-il moins grave que le harcèlement "tout court" ?
+ En cas de harcèlement, que risque-t-on si l'on est mineur ?
+ Vrai ou faux ? Être simplement témoin de harcèlement, sans en être l'auteur⋅e
  initial⋅e, n'est pas grave.
    + Pourquoi ?
    + Que faut-il faire si vous êtes témoin de (cyber)harcèlement ?
+ Que peut-on faire pour éviter les situations de cyberharcèlement ?
+ Comment faire pour supprimer du contenu en ligne ?
+ Vrai ou faux ? Sur Internet, il est impossible de retrouver les auteur⋅es de
  cyberharcèlement à cause de l'anonymat (pseudonymes, *etc.*).
