## Un peu de CSS

### Première partie

1. Commencez par [télécharger le dossier Ressources](https://erou.forge.aeif.fr/snt/zips/ressources.zip).
2. Créez un dossier `Premiers pas CSS` sur votre machine.
3. Décompressez le fichier `ressources.zip` dans le dossier `Premiers pas CSS`.
4. Allez dans le dossier `Premiers pas CSS` et ouvrez le fichier `index.html`
   avec Mozilla Firefox.
5. Ouvrez également ce même fichier avec un éditeur de texte (type « Bloc Note
   » ou « Notepad++ »)
6. Faites le lien entre les balises HTML et la structure de la page dans le
   navigateur web.
7. Enlevez le commentaire ligne 8, autour de la balise `<link>`. Cette balise
   permet de faire le lien avec le fichier `style.css`.
8. Ouvrez à nouveau le fichier `index.html` dans le navigateur et constatez le
   changement. Pourquoi a-t-il eu lieu ?
9. Ouvrez à présent le fichier `style.css` et analysez la syntaxe.
10. Effectuez les changements suivant sur la page, en éditant le fichier CSS.
    - Le titre de niveau 1 doit être en rouge, souligné et centré.
    - Le titre de niveau 2 doit être bleu, en italique et aligné à droite.
    - Les paragraphes doivent être écrits en vert.

*Pour la dernière étape, vous êtes encouragé⋅e à vous aider de recherches sur le
web.*

### Deuxième partie

On va maintenant modifier les fichiers HTML et CSS pour créer votre propre CV,
ou bien celui d'une personne célèbre.

Vous devez :
- ajouter des images
- ajouter des paragraphes
- mettre un titre et un sous-titre
- mettre des propriétés CSS

*Vous êtes encouragé⋅e à faire preuve de créativité, à rechercher sur le web, et
à regarder le fichier .pdf « Ressources CSS » présent dans le dossier
Ressources.*
