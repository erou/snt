## Introduction

Après avoir visionné la vidéo ci-dessous, faites-en un résumé de quelques
lignes.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/9UMvyfT4V_Y?si=NIy1DWKePniYKHlr" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

### Activité sur le binaire

[Lien vers l'activité](https://erou.forge.apps.education.fr/snt/pdfs/activite-binaire.pdf)
